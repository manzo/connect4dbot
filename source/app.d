import std.stdio;
import std.string;
import std.conv;
import boards;
	
void main()
{
    string line;

	//auto board=new Board();
	auto board=new RandomBoard();
	//auto board=new SequentialBoard();
	//auto board=new MirrorBoard();
	//auto board=new SmartBoard();

    while ((line = stdin.readln()) !is null)
    {
		if (line.length == 0) continue;
        auto tokens=line.strip.split;
		if (tokens.length==0) continue;
        switch (tokens[0]) {
			case "update":
				if (tokens[1]!="game") continue;
				if (tokens[2]=="round") {
					board.setRound(parse!int(tokens[3]));
					continue;
				}
				if (tokens[2]=="field")  {
					board.updateField(tokens[3].split(","));
					continue;
				}

			break;
			case "settings":
				switch(tokens[1]) {
					case "field_width":
						board.setWidth(parse!int(tokens[2]));
					break;
					case "field_height":
						board.setHeight(parse!int(tokens[2]));
					break;
					case "your_botid":
						board.setBotId(parse!int(tokens[2]));
					break;
					default:
						continue;
				}
			break;
			case "action":
				writeln("place_disc ",board.move());
			break;
			default:
				stderr.writeln("command not valid:",tokens);

		}
		//stderr.writeln(board.dump());
		stdout.flush();
		stderr.flush();
    }
        
}
