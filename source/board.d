import std.array;
import std.conv;
import std.stdio;


class Board {

    protected:
        int width,height,botId,round;
    
        char[42] field;
        char[42] old_field;

    public:
    void setWidth(const int n) {this.width=n;}
    void setHeight(const int n) {this.height=n;}
    void setBotId(const int n) {this.botId=n;}
    void setRound(const int n) {this.round=n;}

    void updateField(const string[] newstate){
        for(int i=0; i<width*height; i++)  {
            old_field[i]=field[i];
        }
        //stderr.writeln("update: old_field=",old_field,"field=",field);
        //field.length=0;
        auto i=0;
        foreach(c;newstate) {
            field[i++]=to!char(c);
        }
    }

    void init_field() {
        for(int i=0; i<width*height; i++)  {
            field[i]='.';
            old_field[i]='.';
        }
    }


    // mossa di default, fare override nelle classi derivate
    int move() {
        return available_moves[0];
    }

    //ritorna una lista delle mosse disponibili
    int[] available_moves() {
        int[] result;
        if (field.length==0) return result;
        for(int i=0;i<width;i++) {
            if (field[i]=='.') result~=i;
        }
        return result;
    }

    //rappresenta la scacchiera in una stringa a scopo debug
    string dump() {
        auto strBuilder = appender!string;
        int x;
        for (int i=0;i<field.length;i++) {
            strBuilder.put(field[i]);
            x++;
            if (x==width) {
                strBuilder.put("\n");
                x=0;
            }
        }
        return strBuilder.data;
    }
    
}