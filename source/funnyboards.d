import board;


// muove a caso tra le caselle libere
class RandomBoard : Board {
    override int move() {
        import std.random: choice;
        return choice(available_moves());
    }
}

// muove sempre in sequenza 0123456 0123456 ecc
class SequentialBoard : Board {

    private:
        int lastmove=-1;
    
    public:

    override int move() {
        lastmove++;
        return lastmove%width;
    }

}
