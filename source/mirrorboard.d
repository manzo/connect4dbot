import board;
import std.stdio;


// muove "specchiando" l'ultima mossa 
class MirrorBoard : Board {
    override int move() {
        //stderr.writeln("move1: old_field=",old_field,"field=",field);
        if (field.length==0) init_field();
        //stderr.writeln("move2: old_field=",old_field,"field=",field);
        auto col=comparestates() % this.width;
        return this.width-1-col;
    }

    private:
        //confronta vecchio e nuovo per trovare l'ultima mossa
        int comparestates() {
            auto found=-1;
            for(auto i=0; i<(this.width*this.height); i++){
                //stderr.writeln("i=",i,"field[i]=",field[i]);
                if(field[i]!=old_field[i]) { 
                    found=i;
                    break;
                }
            }
            if (found<7) { // in prima riga
                import std.random: choice;
                return choice(available_moves());
            }
            return (found==-1)?this.width/2:found;
        }

}

