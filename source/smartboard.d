import board;
import std.stdio;


// muove con un algoritmo 
class SmartBoard : Board {

public:
    override int move() {
        char player;
        skill=7;
        // varia profondità ricerca in base all'avanzamento della partita
        if (this.round<3) skill=4;
        if (this.round>5) skill=9; 
        if (field.length==0) init_field(); 
        if (this.botId==0)  player='0';
        if (this.botId==1)  player='1';
        return best_move(player);
    }

private: 
    long nodes;
    int skill;

//assegna un punteggio alla scacchiera
int stimapunteggio(const char player)
{
    int value;
    for(int l=2;l<4;l++)
        for(int i;i<this.width;i++)
            if(lungoalmeno(player,i,l)) value+= l;
    return value;
}
 
int valutamossa(const char player,const int depth,const int column,const int trigger)
{
    int max = -200;
    auto otherplayer=(player=='1')?'0':'1';
    if (lungoalmeno(otherplayer,column,4)) return -128;
    if (depth==0) return 0;
    for(int i;i<width;i++)
    {
        if(field[i]=='.')
        {
            auto j = this.height-1;
            while(field[i+j*width]!='.') j--;
            field[i+j*width] = player;
            nodes++;
            const value = -valutamossa(otherplayer,depth-1,i,-max)/2;
            field[i+j*width] = '.';
            if (value>max) max = value;
            if (value>trigger) return max;
        }
    }
    return max;
}
 
int best_move(const char player)
{
    int i,j,value;
    auto otherplayer=(player=='1')?'0':'1';
    int max = -100;
    int best = -1;
    for(i=0;i<width;i++)
    {
        if(field[i]=='.')
        {
            //stderr.writeln("considering column",i);
            nodes = 0;
            j = this.height-1;
            while((field[i+j*width]!='.')&&(j>=0)) j--;
            field[i+j*width] = player; // fa la mossa
            value = -valutamossa(otherplayer,skill,i,200);
            
            //stderr.writeln("value=",value);
            field[i+j*width] = '.';  // rimette-a-posto-la-candela
            if (value>max)
            {
                max = value;
                best = i;
            }
        }
    }

    //tutte le mosse hanno lo stesso punteggio; scelgo una a caso valida
    if(best==-1)
    {
        import std.random: choice;
        return choice(available_moves());
        //for(i=width-1;i>=0;i--) if(field[i]=='.') return i;
    }
 
    return best;
}



//ritorna vero se player ha almeno length pedine consecutive in column
bool lungoalmeno(const char player,const int column,int length)
{
    length--;
    int row = 0;
    while(field[column+row*this.width]=='.') row++;
    if (orizzontale(player,column,row)>=length) return true;
    //conta in verticale verso il basso (verso l'alto non ci sono pedine)
    if (verticale(player,column,row)>=length) return true;
     //conta diagonale \ basso-destra
    if (diagonale1(player,column,row)>=length) return true;
    // calcola diagonale \ alto-sinistra
    if (diagonale2(player,column,row)>=length) return true;
    return false;
}

//conta in orizzontale
int orizzontale(immutable char player, immutable int column, immutable int row) {
    int count=0;
    for (int i=column;(++i)<this.width && field[i+row*this.width]==player;) count++;
    for (int i=column;(--i)>=0 && field[i+row*this.width]==player;) count++;
    return count;
}

int verticale(immutable char player, immutable int column, immutable int row) {
    int count;
    for (int i=row;(++i)<this.height && field[column+i*this.width]==player;) count++;
    return count;
}

//conta diagonale \ alto sinistra basso destra
int diagonale1(immutable char player, immutable int column, immutable int row) {
    int count=0;
    for (int i=column,j=row;(++i)<this.width && (++j)<this.height && field[i+j*this.width]==player;) count++;
    for (int i=column,j=row;(--i)>=0 && (--j)>=0 && field[i+j*this.width]==player;) count++;
    return count;
}

//conta diagonale / basso sinistra - alto destra
int diagonale2(immutable char player, immutable int column, immutable int row) {
    int count=0;
    for (int i=column,j=row;(++i)<this.width && (--j)>=0 && field[i+j*this.width]==player;) count++;
    for (int i=column,j=row;(--i)>=0 && (++j)<this.height && field[i+j*this.width]==player;) count++;
    return count;
}

}
